#!/bin/bash

function print_usage {
    echo "usage: $1 org lab_prefix"
}

# Check basic arguments
if [ "$#" -lt 2 ]
then
    print_usage $0
    exit 1
fi

current_dir=`dirname $0`
org=$1
lab=$2

echo $org
hub api --paginate orgs/$1/repos|jq -r .[].name|rg $2|sed "s/$2-//g"
