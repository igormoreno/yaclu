import os.path
import argparse

parser = argparse.ArgumentParser(description='Checks if a set of files exist in each repository and outputs a CSV text with \'yes\' or \'no\' for each file and each repository')
parser.add_argument('meta_file', help='GitHub classroom meta file')
parser.add_argument('repo_prefix', help='Prefix of the repostitory. Typically the assignment name sometimes preceded of another prefix. For example pf1-lab-1-praticamentetilde has prefix pf1-lab-1')
parser.add_argument('files', nargs='+', help='Files to search for')

args = parser.parse_args()

def done(filen):
  if os.path.isfile(filen):
    return "yes"
  else:
    return "no"

header = "github_id," + ",".join(args.files)
print(header)
lines = [line.rstrip('\n') for line in open(args.meta_file)]
for name in lines[1:]:
    line = name + "," + ",".join([done(args.repo_prefix+"-"+name+"/"+file_name) for file_name in args.files])
    print(line)

