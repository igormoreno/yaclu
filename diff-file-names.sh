#!/bin/zsh

prefix_size=11 # in characters

function print_usage {
    echo "usage: $1 commit"
}

# Check basic arguments
if [ "$#" -lt 1 ]
then
    print_usage $0
    exit 1
fi

commit=$1

echo "repo,mod,file"
for f in *lab*; do
    cd $f
    username=`echo $f|cut -c $prefix_size-`
    git --no-pager diff --name-status $commit|sed "s/	/,/g"|xargs -I{} echo "$username,{}"
    cd ..
done

