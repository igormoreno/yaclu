#!/bin/bash

function print_usage {
    echo "usage: $1 org lab_prefix"
}

# Check basic arguments
if [ "$#" -lt 2 ]
then
    print_usage $0
    exit 1
fi

org=$1
lab=$2

for f in lab-*; do
  echo "======== $f"
  echo $lab > $f/README.md
  echo "============" >> $f/README.md
  echo "[![Build Status](https://travis-ci.com/$org/$f.svg?token=PfcGoYv47s1MxsyK7ZUh&branch=master)](https://travis-ci.com/$org/$f)" >> $f/README.md
done

