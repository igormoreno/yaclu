use structopt::StructOpt;
use std::path::PathBuf;

/// Checks if a set of files exist in each repository and outputs a CSV text with \'yes\' or \'no\' for each file and each repository.
#[derive(Debug, StructOpt)]
struct Cli {
    /// GitHub classroom meta file
    #[structopt(parse(from_os_str))]
    meta_file: PathBuf,
    /// Prefix of the repostitory. Typically the assignment name sometimes preceded of another prefix. For example pf1-lab-1-praticamentetilde has prefix pf1-lab-1
    repo_prefix: String,
    /// Files to search for
    files: Vec<String>,
}

fn main() {
    let args = Cli::from_args();

    print!("github_id");
    for file in args.files.iter() {
        print!(",{}",file);
    }
    println!();

    let content = std::fs::read_to_string(&args.meta_file).expect("could not read file");
    for github_id in content.lines().skip(1) {
        print!("{}",github_id);
        for file in args.files.iter() {
            // let path = base_folder / args.repo_prefix - github_id / file
            let mut path: PathBuf = args.meta_file.parent().map(|x|x.to_path_buf()).unwrap();
            path.push(args.repo_prefix.clone() + "-" + github_id);
            path.push(file);
            let is_done = if path.is_file() { "yes" } else { "no" };
            print!(",{}",is_done);
        };
        println!();
    };
}
