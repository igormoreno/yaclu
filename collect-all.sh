#!/bin/bash

function print_usage {
    echo "usage: $1 org lab_prefix"
}

# Check basic arguments
if [ "$#" -lt 2 ]
then
    print_usage $0
    exit 1
fi

current_dir=`dirname $0`
org=$1
lab=$2

# get repo names from github api
echo "==== getting repo names from github api"
$current_dir/get-assignment-repos.sh $org $lab |tee meta.txt

# clone/update repos
echo "==== clone/update repos"
$current_dir/../classroom-util/classroom.sh collect meta.txt $lab

