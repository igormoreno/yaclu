from github import Github
import argparse

parser = argparse.ArgumentParser(description='Use the GitHub API to get the names of the repositories of an organization and select only the names that start with a given prefix.')
parser.add_argument('organization', help='GitHub organization')
parser.add_argument('repo_prefix', help='Prefix of the repostitory. Typically the assignment name sometimes preceded of another prefix. For example pf1-lab-1-praticamentetilde has prefix pf1-lab-1')

args = parser.parse_args()

g = Github("YOUR_KEY_HERE")

org = args.organization
prefix = args.repo_prefix

print(org)
for repo in g.get_organization(org).get_repos():
  if repo.name.startswith(prefix):
    print(repo.name.replace(prefix + "-","",1))
