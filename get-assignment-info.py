from github import Github
import argparse
import re

parser = argparse.ArgumentParser(description='Use the GitHub API to get the names of the repositories of an organization, group them by assignment and its teams')
parser.add_argument('organization', help='GitHub organization')
parser.add_argument('repo_prefix', help='Prefix of the repostitory. Typically what comes before the assignment name. For example pf1-lab-1-praticamentetilde has prefix pf1')
parser.add_argument('--assignment_prefix', help='Prefix of the assignment. For example pf1-lab-1-praticamentetilde has prefix lab-1')
parser.add_argument('--csv', action='store_true', help='Output in csv format')

args = parser.parse_args()

g = Github("YOUR-KEY-HERE")

org = args.organization
prefix = args.repo_prefix

repos = g.get_organization(org).get_repos()
teams = {t.slug:t for t in g.get_organization(org).get_teams()}

assignments = set()
if args.assignment_prefix is None:
  for repo in repos:
    match = re.search(prefix + "-([a-z]+[-a-z0-9]+)", repo.name)
    if match:
      assignments.add(match.group(1))
else:
  assignments.add(args.assignment_prefix)


if args.csv is True:
  print("assignment,repository,group,student")
else:
  print("assignments: " + str(assignments))
for assignment in assignments:
  if args.csv is False:
    print("assignment: " + str(assignment))
  for repo in repos:
    match = re.search("{0}-{1}-(.*)".format(prefix, str(assignment)), repo.name)
    if match:
      if match.group(1) in teams:
        team = match.group(1)
        members = [m.login for m in teams[team].get_members()]
        if args.csv is True:
          for member in members:
            print("{0},{1},{2},{3}".format(str(assignment),repo.name,team,member))
        else:
          print("  {0} (group: {1})".format(repo.name, str(members)))
      else:
        if args.csv is True:
          student = match.group(1)
          print("{0},{1},,{2}".format(str(assignment),repo.name,student))
        else:
          print("  {0}".format(repo.name))
