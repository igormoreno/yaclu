from os import listdir
from os.path import isfile, join
import argparse

parser = argparse.ArgumentParser(description='Checks if a set of files exist in each repository and writes feedback complaining about naming conventions')
parser.add_argument('meta_file', help='GitHub classroom meta file')
parser.add_argument('repo_prefix', help='Prefix of the repostitory. Typically the assignment name sometimes preceded of another prefix. For example pf1-lab-1-praticamentetilde has prefix pf1-lab-1')

args = parser.parse_args()

feedback_file = "feedback.md"
feedback_text = """# Feedback

- Please follow the file naming conventions:
https://usi-pl.github.io/pf1-2018/policies.html

"""

expected_files = ["01.rkt", "02.rkt", "03.rkt", "04.rkt", "coffee.rkt", "README.md", "feedback.md", ".gitignore"]

def ifBad(folder):
    print(folder + " is wrong")
    with open(join(folder, "feedback.md"), "a") as feedback_file:
        feedback_file.write(feedback_text)

github_usernames = [line.rstrip('\n') for line in open(args.meta_file)][1:]
num_bad = 0
for name in github_usernames:
    folder_name = args.repo_prefix+"-"+name
    onlyfiles = [f for f in listdir(folder_name) if isfile(join(folder_name, f))]
    good = set(expected_files) == set(onlyfiles)
    if((not good) and (len(onlyfiles) > 3)):
        num_bad += 1
        ifBad(folder_name)

print("bad: "+str(num_bad)+"/"+str(len(github_usernames))+" = "+str(num_bad/len(github_usernames)))

