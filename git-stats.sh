#!/bin/bash

commit_hash=${1:?usage: $0 commit_hash}

echo "repo,commits,files,insertions,deletions"
for f in lab*; do
    cd $f
    commits=$(git --no-pager shortlog -se $commit_hash..HEAD|awk '{ x += $1 } END { print x }')
    changes=$(git --no-pager diff --shortstat $commit_hash|cut -d' ' -f 2,5,7|sed "s/ /,/g")
    echo $f,$commits,$changes|cut -c 8-
    cd ..
done



