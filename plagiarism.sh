#!/bin/bash

function print_usage {
    echo "usage: $1 [--prefix Prefix | --file Student_file] file"
}

# Check basic arguments
if [ "$#" -lt 3 ]
then
    print_usage $0
    exit 1
fi

file=$3

case $1 in
    --prefix)
    students=`ls |grep $2`
    ;;
    --file)
    students=`cat $2`
    ;;
esac

for f in $students; do
  echo "===== checking $f:"
  for g in $students; do
    if [ $f != $g ]; then
      echo $f $g `wdiff -123 -s $f/$file $g/$file 2> /dev/null|grep -o "[0-9]*% common"`|sed 's/%//g'|awk '{ if ($3 > 75) { print } }'
    fi
  done
done


